module bitbucket.org/uwaploe/mpcmgr

go 1.20

require (
	bitbucket.org/mfkenney/go-redisq/v2 v2.0.1
	bitbucket.org/uwaploe/go-dpcmsg/v2 v2.4.1
	bitbucket.org/uwaploe/go-dputil v1.4.1
	bitbucket.org/uwaploe/go-mpc v1.3.1
	bitbucket.org/uwaploe/tsfpga v0.7.1
	github.com/gomodule/redigo v1.8.5
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	google.golang.org/grpc v1.40.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
