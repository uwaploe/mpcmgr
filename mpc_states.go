// State machine for the MPC
package main

import (
	"container/list"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	redisq "bitbucket.org/mfkenney/go-redisq/v2"
	dpcmsg "bitbucket.org/uwaploe/go-dpcmsg/v2"
	dputil "bitbucket.org/uwaploe/go-dputil"
	mpc "bitbucket.org/uwaploe/go-mpc"
	"bitbucket.org/uwaploe/mpcmgr/internal/fwddiff"
	"bitbucket.org/uwaploe/mpcmgr/internal/ips"
	"bitbucket.org/uwaploe/mpcmgr/internal/power"
	"bitbucket.org/uwaploe/mpcmgr/internal/wlan"
	"github.com/gomodule/redigo/redis"
)

type MpcState struct {
	m                     *mpc.MPC
	conn                  redis.Conn
	sw                    power.Switch
	charger               *ips.Ips
	err                   error
	cfg                   ProgCfg
	profile               mpc.ProfileConfig
	tstart                time.Time
	docked                bool
	dataq                 *redisq.Queue
	profileqs             *list.List
	lastRec               *dputil.DataRecord
	inProfile             bool
	stallCount, maxStalls int
	maxPr                 float64
	dpdt                  *fwddiff.Diff
}

// Error triggered by an invalid state change
type StateError struct {
	msg string
}

func (e *StateError) Error() string {
	return e.msg
}

type Handler func(context.Context, *MpcState) string

type Fsm struct {
	Handlers   map[string]Handler
	StartState string
	EndStates  map[string]bool
}

var mmpModes = map[mpc.ProfileType]string{
	mpc.PROFILE_DOWN:       "down",
	mpc.PROFILE_UP:         "up",
	mpc.PROFILE_STATIONARY: "stationary",
	mpc.PROFILE_DOCKING:    "docking",
}

// Convert an MMPPOS message into a Deep Profiler DataRecord
func newMmpData(msg mpc.Message) *dputil.DataRecord {
	pos := mpc.Position{}
	if err := mpc.UnmarshalMpc(msg, &pos); err != nil {
		return nil
	}
	return &dputil.DataRecord{
		Source: "mmp_1",
		T:      time.Unix(pos.T, 0),
		Data:   pos.AsMap(),
	}
}

// Convert various MPC messages into Events.
func newMmpEvent(msg mpc.Message) *dputil.Event {
	ev := dputil.Event{}

	switch msg.Cmd {
	case "MMPPRF":
		obj := mpc.ProfStart{}
		if err := mpc.UnmarshalMpc(msg, &obj); err != nil {
			return nil
		}
		ev.Name = "profile:start"
		ev.Attrs = obj.AsMap()
	case "MMPEND":
		obj := mpc.ProfEnd{}
		if err := mpc.UnmarshalMpc(msg, &obj); err != nil {
			return nil
		}
		ev.Name = "profile:end"
		ev.Attrs = obj.AsMap()
	case "MMPDCK":
		obj := mpc.ProfDock{}
		if err := mpc.UnmarshalMpc(msg, &obj); err != nil {
			return nil
		}
		ev.Name = "docked"
		ev.Attrs = obj.AsMap()
	case "MMPOBS":
		obj := mpc.ProfObs{}
		if err := mpc.UnmarshalMpc(msg, &obj); err != nil {
			return nil
		}
		ev.Name = "stalled"
		ev.Attrs = obj.AsMap()
	default:
		ev.Name = msg.Cmd
	}

	return &ev
}

func newWaitEvent(d time.Duration, state string) *dputil.Event {
	ev := &dputil.Event{
		Name:  "wait",
		Attrs: make(map[string]interface{})}
	ev.Attrs["t"] = int64(time.Now().Unix())
	ev.Attrs["duration"] = int64(d / time.Second)
	ev.Attrs["state"] = state
	return ev
}

func inDock() bool {
	stats, err := wlan.GetStats("wlan0")
	if err != nil {
		return false
	}

	return stats.Level > -28
}

// Handle the various MPC messages appropriately. MMPPOS (status) messages
// are sent to the archive while the others are published on a Redis
// pub-sub channel.
func dispatchMsg(msg mpc.Message, ms *MpcState) {
	switch msg.Cmd {
	case "MMPPRF":
		ms.docked = false
		ms.inProfile = true
		ev := newMmpEvent(msg)
		if ev != nil {
			log.Println(ev)
			ms.conn.Do("PUBLISH", ms.cfg.Channel, ev.String())
			ms.conn.Do("HSET", "mpc", "pnum", ev.Attrs["pnum"])
			ms.conn.Do("HSET", "mpc", "mode", ev.Attrs["mode"])
		}
		ms.m.Send("ACKPRF", int64(1))
	case "MMPPOS":
		dr := newMmpData(msg)
		if dr != nil {
			ms.lastRec = dr
			data := dr.Data.(map[string]interface{})
			if pr, ok := data["pressure"].(float64); ok {
				if pr > ms.maxPr {
					ms.maxPr = pr
				}
				data["speed"] = ms.dpdt.Next(pr)
				dr.Data = data
			}
			v := dr.Serialize()
			err := ms.dataq.Put(v...)
			if err != nil {
				log.Printf("Cannot store data record: %v\n", err)
			}
		} else {
			log.Printf("Invalid MMPPOS message: %v", msg.Params)
		}
		ms.docked = false
	case "MMPOBS":
		if ev := newMmpEvent(msg); ev != nil {
			log.Println(ev)
			ms.conn.Do("PUBLISH", ms.cfg.Channel, ev.String())
		}
	case "MMPDCK":
		ms.docked = true
		if ev := newMmpEvent(msg); ev != nil {
			log.Println(ev)
			ms.conn.Do("PUBLISH", ms.cfg.Channel, ev.String())
		}
		ms.m.Send("ACKDCK")
	case "MMPEND":
		if ev := newMmpEvent(msg); ev != nil {
			log.Println(ev)
			ms.conn.Do("PUBLISH", ms.cfg.Channel, ev.String())
			// Decode the termination status code.
			if status, found := ev.Attrs["status"]; found {
				switch code := status.(int64); code {
				case 22:
					ms.docked = true
				case 15, 16:
					if ms.profile.Direction == mpc.PROFILE_DOCKING {
						// The limit switch might have failed, in which case
						// the dock would be detected as an obstruction. Use
						// the WiFi signal strength to determine if we are
						// actually in the dock.
						if inDock() {
							ms.docked = true
						}
					} else {
						// Retry the profile unless there have been an excessive
						// number of stalls.
						if !ms.docked && ms.stallCount > 0 {
							ms.stallCount--
							ms.m.Send("ACKEND")
							return
						}
					}
				}
			}
		}
		ms.stallCount = ms.cfg.Ops.Stalls

		// Create a fake data record to indicate that the profile
		// is complete. We re-queue the most recent data record with
		// the mode set to "stopped" and the time-stamp set to now
		if ms.lastRec != nil {
			rec, ok := ms.lastRec.Data.(map[string]interface{})
			if ok {
				ms.lastRec.T = time.Now()
				if ms.docked {
					rec["mode"] = "docked"
				} else {
					rec["mode"] = "stopped"
				}
				v := ms.lastRec.Serialize()
				err := ms.dataq.Put(v...)
				if err != nil {
					log.Printf("Cannot store data record: %v\n", err)
				}
			} else {
				log.Printf("Invalid data record type\n")
			}
		}
		ms.m.Send("ACKEND")
		ms.inProfile = false
	}
}

// Monitor a profile and handle each message sent by the MPC. Returns any
// error that occurs.
func monitorProfile(ctx context.Context, ms *MpcState) error {
	c, errc := ms.m.Reader(ctx, "MMPRDY", false)

	for msg := range c {
		dispatchMsg(msg, ms)
	}

	// Check for errors from the reader
	err := <-errc
	return err
}

// Insert the current profile at the head of the queue
func requeueProfile(ms *MpcState) error {
	msg := dpcmsg.ProfileMessage{Cfg: &ms.profile}
	b, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	_, err = ms.conn.Do("LPUSH", ms.cfg.Queues["profile"], b)
	if err != nil {
		return err
	}

	_, err = ms.conn.Do("LPUSH", ms.cfg.Queues["command"], "go")

	return err
}

// Process a command and return the next FSM state
func processCommand(cmd string, ms *MpcState) string {
	switch cmd {
	case "dock":
		// Create a docking profile and prepend to the queue
		p := mpc.ProfileConfig{Direction: mpc.PROFILE_DOCKING}
		msg := dpcmsg.ProfileMessage{Cfg: &p}
		b, err := json.Marshal(msg)
		if err == nil {
			ms.conn.Do("LPUSH", ms.cfg.Queues["profile"], b)
		}
		return "rdprofile"
	case "go":
		return "rdprofile"
	case "halt":
		return "shutdown"
	case "undock", "continue":
		return "wait"
	default:
		ms.err = fmt.Errorf("Unknown command %q\n", cmd)
	}

	return "error"
}

// Get the next profile definition from the queue.
func mpcRdprofile(ctx context.Context, ms *MpcState) string {
	log.Println("Check profile queue")
	e := ms.profileqs.Front()
	qname := e.Value.(string)

	ms.profile = mpc.ProfileConfig{Direction: mpc.PROFILE_INVALID}
	entry, err := redis.Bytes(ms.conn.Do("LPOP", qname))
	if err != nil {
		// This profile queue is empty. Pop it off the stack
		// unless it is the last one.
		if ms.profileqs.Len() > 1 {
			ms.profileqs.Remove(ms.profileqs.Front())
			log.Printf("Switching to queue %q\n",
				ms.profileqs.Front().Value.(string))
			return "rdprofile"
		}
		return "wait"
	}

	msg := dpcmsg.ProfileMessage{}
	err = json.Unmarshal(entry, &msg)
	if err != nil {
		// Is this entry the name of another queue?
		name := string(entry)
		exists, err := redis.Int(ms.conn.Do("EXISTS", name))
		if err == nil && exists == 1 {
			// Get the next profile from the new queue.
			log.Printf("Switching to queue %q\n", name)
			ms.profileqs.PushFront(name)
			return "rdprofile"
		}
		log.Printf("Bad profile-queue entry: %q\n", name)
		return "wait"
	}

	ms.profile = *msg.Cfg
	if ms.profile.Requeue > 0 {
		msg.Cfg.Requeue = msg.Cfg.Requeue - 1
		b, err := json.Marshal(&msg)
		if err == nil {
			log.Printf("Requeuing profile description\n")
			ms.conn.Do("RPUSH", qname, b)
		}
	}

	// Update list of excluded sensors
	lname := "sens:exclude"
	if len(msg.Exclude) > 0 {
		args := make([]interface{}, 0)
		args = append(args, lname)
		for _, s := range msg.Exclude {
			args = append(args, s)
		}
		ms.conn.Send("DEL", lname)
		_, err = ms.conn.Do("RPUSH", args...)
	} else {
		_, err = ms.conn.Do("DEL", lname)
	}

	is_on, _ := ms.sw.Test()
	if is_on {
		return "ready"
	}
	return "startup"
}

func mpcWait(ctx context.Context, ms *MpcState) string {
	is_on, _ := ms.sw.Test()
	if is_on {
		log.Println("Powering off")
		ms.sw.Off()
	}
	log.Println("Waiting for a command")
	ev := newWaitEvent(time.Duration(0), "wait")
	ms.conn.Do("PUBLISH", ms.cfg.Channel, ev.String())

loop:
	for {
		select {
		case <-ctx.Done():
			ms.err = ctx.Err()
			break loop
		default:
			reply, err := redis.Values(ms.conn.Do("BLPOP",
				ms.cfg.Queues["command"], 3))
			if err == nil {
				cmd := string(reply[1].([]byte))
				log.Printf("Got command: %q\n", cmd)
				return processCommand(cmd, ms)
			}
		}
	}

	return "error"
}

func mpcCheck(ctx context.Context, ms *MpcState) string {
	// MPC restarted or stalled in the middle of a profile, rerun it.
	if ms.inProfile {
		log.Println("MPC stall/restart detected. Reloading current profile")
		ms.inProfile = false
		return "ready"
	}

	log.Println("Check command queue")
	cmd, err := redis.String(ms.conn.Do("LPOP", ms.cfg.Queues["command"]))
	if err == nil {
		return processCommand(cmd, ms)
	}

	return "rdprofile"
}

func mpcStartup(ctx context.Context, ms *MpcState) string {
	ms.sw.On()
	ms.m.SetTimeout(time.Second * 70)
	ms.stallCount = ms.cfg.Ops.Stalls

	log.Printf("Power on\n")
	ms.err = ms.m.WaitUntilReady()
	if ms.err != nil {
		return "error"
	}
	ms.m.SetTimeout(time.Second * 30)

	ms.err = ms.m.SetRamp(ms.cfg.Ops.Ramp)
	if ms.err != nil {
		return "error"
	}

	ms.err = ms.m.SetClock(mpcTimeOffset)
	if ms.err != nil {
		return "error"
	}

	ms.err = ms.m.SetWarmUp(warmupTime)
	if ms.err != nil {
		return "error"
	}

	return "ready"
}

func mpcReady(ctx context.Context, ms *MpcState) string {
	switch ms.profile.Direction {
	case mpc.PROFILE_INVALID:
		log.Printf("Invalid profile: %s", ms.profile.String())
		return "wait"
	case mpc.PROFILE_DOCKING:
		// Load a DOWN profile to ensure that the deep limit
		// is set before the docking command is sent.
		p := mpc.ProfileConfig{
			Direction:  mpc.PROFILE_DOWN,
			StartTime:  0,
			Deep:       3000,
			Shallow:    0,
			StopCheck:  5,
			TimeLimit:  0,
			ShallowErr: 2,
			DeepErr:    2,
		}
		log.Printf("Prepare for docking\n")
		ms.m.SetTimeout(time.Second * 30)
		log.Print("Adjusting back-track parameters")
		ms.err = ms.m.SetBacktrack(mpc.BacktrackConfig{
			Iterations: 1,
			Rate:       45,
			Time:       10,
		})
		// Load profile parameters
		ms.m.SetProfile(p)
		if err := ms.m.GotoDock(); err != nil {
			ms.err = fmt.Errorf("GotoDock failed: %w", err)
			return "error"
		}
		return "profile"
	default:
		log.Printf("Set-up profile: %s\n", ms.profile.String())
		ms.m.SetBacktrack(mpc.BacktrackConfig{
			Iterations: 5,
			Rate:       45,
			Time:       60,
		})
		// A long timeout is required for profile start-up
		ms.m.SetTimeout(time.Second * 70)
		log.Printf("Sync clock\n")
		if err := ms.m.SetClock(mpcTimeOffset); err != nil {
			ms.err = fmt.Errorf("SetClock failed: %w", err)
			return "error"
		}

		// Load profile parameters
		if ts, err := ms.m.SetProfile(ms.profile); err != nil {
			ms.err = fmt.Errorf("SetProfile failed: %w", err)
			log.Println("Requeueing profile")
			requeueProfile(ms)
			return "error"
		} else {
			ms.tstart = ts
		}

		log.Printf("Request profile start (%s)\n", ms.tstart.Format(time.RFC3339))
		if err := ms.m.StartProfile(); err != nil {
			ms.err = fmt.Errorf("StartProfile failed: %w", err)
			return "error"
		}

		// If a delayed start has been requested, the MPC will suspend the telemetry
		// session and go into a low-power sleep mode until the start time.
		if ms.tstart.After(time.Now()) {
			return "delay"
		}
		return "profile"
	}
}

func mpcDelay(ctx context.Context, ms *MpcState) string {
	delay := ms.tstart.Sub(time.Now()) - (time.Second * 5)
	if delay > time.Duration(0) {
		// Signal subscribers that we are waiting
		ev := newWaitEvent(delay, "delay")
		ms.conn.Do("PUBLISH", ms.cfg.Channel, ev.String())
		log.Printf("Delayed profile start, sleeping %s\n", delay)
		time.Sleep(delay)
	}
	return "profile"
}

func mpcProfile(ctx context.Context, ms *MpcState) string {
	log.Printf("Monitoring profile\n")

	ms.charger.DisableXformer()
	ms.m.SetTimeout(time.Duration(60) * time.Second)
	ms.err = monitorProfile(ctx, ms)
	if ms.err != nil {
		log.Println("Requeuing current profile")
		requeueProfile(ms)
		return "error"
	}

	// If we were cancelled, transition to the shutdown state,
	// otherwise proceed as normal.
	select {
	case <-ctx.Done():
		return "shutdown"
	default:
		if ms.docked {
			ms.charger.EnableXformer()
			return "docked"
		}
		return "check"
	}
}

func mpcDocked(ctx0 context.Context, ms *MpcState) string {
	next_state := "error"
	// Signal subscribers that we are waiting
	ev := newWaitEvent(time.Duration(0), "docked")
	ms.conn.Do("PUBLISH", ms.cfg.Channel, ev.String())

	ms.m.SetTimeout(time.Duration(5) * time.Second)

	ctx, cancel := context.WithCancel(ctx0)
	defer cancel()

	// Read the output from the MPC until we see an MMPPRF message
	// which indicates that the profiler has drifted out of the
	// Dock and a new profile has been started to get back in.
	c, errc := ms.m.Reader(ctx, "MMPPRF", true)
loop:
	for {
		select {
		case msg, more := <-c:
			if more {
				dispatchMsg(msg, ms)
			} else {
				// Reader closes the channel after sending
				// the MMPPRF message.
				next_state = "profile"
				break loop
			}
		case ms.err = <-errc:
			if ms.err != nil {
				break loop
			}
		case <-ctx.Done():
			next_state = "shutdown"
			ms.err = ctx.Err()
			break loop
		case <-time.After(time.Second * 5):
			cmd, err := redis.String(ms.conn.Do("LPOP",
				ms.cfg.Queues["command"]))
			if err == nil {
				next_state = processCommand(cmd, ms)
				ms.m.LeaveDock()
				break loop
			}
		}
	}

	// Signal the Reader to exit ...
	cancel()
	// ... and wait for it.
	ms.m.ReaderWait()

	return next_state
}

func mpcShutdown(ctx context.Context, ms *MpcState) string {
	ms.sw.Off()
	log.Printf("Power off\n")
	return "done"
}

func mpcError(ctx context.Context, ms *MpcState) string {
	log.Printf("Error: %v\n", ms.err)
	return "shutdown"
}

func (fsm *Fsm) AddState(name string, fn Handler) {
	fsm.Handlers[name] = fn
}

func (fsm *Fsm) AddEndState(name string) {
	fsm.EndStates[name] = true
}

func (fsm *Fsm) Run(ctx context.Context, ms *MpcState) {
	handler := fsm.Handlers[fsm.StartState]
	for {
		next := handler(ctx, ms)
		log.Printf("Next state = %q\n", next)
		ms.conn.Do("HSET", "mpc", "state", next)
		_, done := fsm.EndStates[next]
		if done {
			break
		} else {
			handler = fsm.Handlers[next]
		}
	}
}
