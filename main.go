// This program is part of the Deep Profiler project and manages the
// interface to the McLane Profiler Controller.
package main

import (
	"container/list"
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	redisq "bitbucket.org/mfkenney/go-redisq/v2"
	mpc "bitbucket.org/uwaploe/go-mpc"
	"bitbucket.org/uwaploe/mpcmgr/internal/ema"
	"bitbucket.org/uwaploe/mpcmgr/internal/fwddiff"
	"bitbucket.org/uwaploe/mpcmgr/internal/ips"
	"bitbucket.org/uwaploe/mpcmgr/internal/power"
	pb "bitbucket.org/uwaploe/tsfpga/api"
	"github.com/gomodule/redigo/redis"
	"github.com/tarm/serial"
	"google.golang.org/grpc"
)

const Usage = `Usage: mpcmgr [options] [cfgfile]

Manage the interface with the MPC.

`

const (
	// Apparent MPC command "overhead" time. We need to add this
	// when syncing the clock.
	mpcTimeOffset = 3
	// Warm-up time at profile start
	warmupTime = 10
	// Interval between status messages when profiling
	checkTime = 5
)

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpCfg bool
	doDebug bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.BoolVar(&dumpCfg, "dumpcfg", dumpCfg,
		"dump default configuration to stdout and exit")
	flag.BoolVar(&doDebug, "debug", doDebug,
		"enable diagnostic output")

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func newClient(addr string) (pb.FpgaMemClient, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())
	conn, err := grpc.Dial(addr, opts...)
	if err != nil {
		return nil, err
	}

	return pb.NewFpgaMemClient(conn), nil
}

func main() {
	args := parseCmdLine()
	if dumpCfg {
		fmt.Fprintf(os.Stdout, "---%s\n", defcfg)
		os.Exit(0)
	}

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	var (
		err error
		cfg ProgCfg
	)

	if len(args) >= 1 {
		contents, err := ioutil.ReadFile(args[0])
		if err != nil {
			log.Fatalf("Cannot read file: %v", err)
		}
		cfg, err = parseConfig(contents)
	} else {
		cfg, err = parseConfig(nil)
	}

	fmt.Printf("%#v\n", cfg)

	// Open serial port
	c := &serial.Config{
		Name:        cfg.Port.Device,
		Baud:        cfg.Port.Baud,
		ReadTimeout: time.Second * 2}
	s, err := serial.OpenPort(c)
	if err != nil {
		log.Fatalf("Cannot open serial port: %v", err)
	}

	ms := MpcState{
		m:    mpc.NewMPC(s, time.Second*time.Duration(mpcTimeOffset*2)),
		cfg:  cfg,
		dpdt: fwddiff.New(ema.New(0.1)),
	}

	// Power switch
	cln, err := newClient("localhost:10101")
	if err != nil {
		log.Fatalf("Cannot access TS-FPGA server: %v", err)
	}

	ms.sw = power.NewDio(cln, cfg.Port.Switch)
	// Make sure the power is off
	ms.sw.Off()
	ms.charger = ips.New(cln)

	ms.m.SetDebug(cfg.Port.Debug || doDebug)

	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		log.Fatalf("Cannot access Redis server: %v", err)
	}
	ms.conn = conn
	// Log our process ID
	conn.Do("HSET", "mpc", "pid", os.Getpid())

	// Initialize the data queue which will be read by the archiver.
	ms.dataq, err = redisq.NewQueue(ms.conn, cfg.Queues["data"], 200)
	if err != nil {
		log.Fatalf("Cannot initialize data queue: %v", err)
	}

	// Stack of profile-queue names.
	ms.profileqs = list.New()
	ms.profileqs.PushFront(ms.cfg.Queues["profile"])

	// Initialize the FSM
	machine := Fsm{StartState: "check"}
	machine.Handlers = make(map[string]Handler)
	machine.EndStates = make(map[string]bool)
	machine.AddState("wait", mpcWait)
	machine.AddState("startup", mpcStartup)
	machine.AddState("ready", mpcReady)
	machine.AddState("delay", mpcDelay)
	machine.AddState("profile", mpcProfile)
	machine.AddState("docked", mpcDocked)
	machine.AddState("check", mpcCheck)
	machine.AddState("rdprofile", mpcRdprofile)
	machine.AddState("error", mpcError)
	machine.AddState("shutdown", mpcShutdown)
	machine.AddEndState("done")

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close. If a signal arrives, close the "cancel" channel
	// to cleanly shutdown the FSM.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	machine.Run(ctx, &ms)
}
