package main

import "gopkg.in/yaml.v2"

// Default configuration
var defcfg = `
port:
  device: /dev/ttyAT3
  baud: 19200
  switch: "8160_LCD_D5"
  debug: false
channel: events.mmp
queues:
  profile: "mpc:profiles"
  data: "archive:queue"
  command: "mpc:commands"
ops:
  stalls: 3
  ramp: 30
`

type portCfg struct {
	Device string `yaml:"device"`
	Baud   int    `yaml:"baud"`
	Switch string `yaml:"switch"`
	Debug  bool   `yaml:"debug"`
}

type opsCfg struct {
	Stalls int `yaml:"stalls"`
	Ramp   int `yaml:"ramp"`
}

type ProgCfg struct {
	Port    portCfg
	Channel string
	Queues  map[string]string
	Ops     opsCfg
}

func parseConfig(contents []byte) (ProgCfg, error) {
	cfg := ProgCfg{}
	var err error

	if contents == nil {
		err = yaml.Unmarshal([]byte(defcfg), &cfg)
	} else {
		err = yaml.Unmarshal(contents, &cfg)
	}

	return cfg, err
}
