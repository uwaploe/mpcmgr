// Package ips controls the Inductive Power System (charging system) on the Deep
// Profiler DPC.
package ips

import (
	"context"
	"time"

	pb "bitbucket.org/uwaploe/tsfpga/api"
)

// Digital outputs for IPS control
const (
	PwrDisable     = "8160_DIO_13"
	XformerDisable = "8160_DIO_15"
)

var restartDelay time.Duration = time.Second * 5

type Ips struct {
	client pb.FpgaMemClient
}

func New(client pb.FpgaMemClient) *Ips {
	return &Ips{
		client: client,
	}
}

func dioset(client pb.FpgaMemClient, name string, state pb.DioState) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	msg := &pb.DioMsg{
		Name:  name,
		State: state}
	_, err := client.DioSet(ctx, msg)
	return err
}

func (ips *Ips) EnablePwr() error {
	return dioset(ips.client, PwrDisable, pb.DioState_LOW)
}

func (ips *Ips) EnableXformer() error {
	return dioset(ips.client, XformerDisable, pb.DioState_LOW)
}

func (ips *Ips) DisablePwr() error {
	return dioset(ips.client, PwrDisable, pb.DioState_HIGH)
}

func (ips *Ips) DisableXformer() error {
	return dioset(ips.client, XformerDisable, pb.DioState_HIGH)
}

func (ips *Ips) Restart() error {
	if err := ips.DisablePwr(); err != nil {
		return err
	}
	time.Sleep(restartDelay)
	return ips.EnablePwr()
}
