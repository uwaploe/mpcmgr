// Ema implements an exponential moving average filter
package ema

import (
	"sync"
)

type Number interface {
	~float64 | ~float32
}

type Ema[T Number] struct {
	alpha, beta T
	mu          *sync.RWMutex
	state       T
	initialized bool
}

// New returns a new Ema with smoothing factor alpha.
func New[T Number](alpha T) *Ema[T] {
	f := &Ema[T]{}
	f.alpha = alpha
	f.beta = 1. - alpha
	f.mu = &sync.RWMutex{}
	return f
}

// Update takes a new input value and returns a new filtered value
func (f *Ema[T]) Update(x T) T {
	if !f.initialized {
		f.state = x
		f.initialized = true
	} else {
		f.mu.Lock()
		f.state = f.alpha*x + f.beta*f.state
		f.mu.Unlock()
	}
	return f.state
}

// State returns the current filtered value
func (f *Ema[T]) State() T {
	return f.state
}
