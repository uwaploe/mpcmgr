// Package wlan reads the Linux wireless network statistics from /proc/net/wireless
package wlan

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

const procFile = "/proc/net/wireless"

type Entry struct {
	IfName    string
	Status    uint
	Qual      float64
	Level     int
	Noise     int
	Discarded struct {
		Nwid  int
		Crypt int
		Frag  int
		Retry int
		Misc  int
	}
	Missed int
}

func scanEntry(input string) (Entry, error) {
	var e Entry
	_, err := fmt.Sscanf(input,
		"%s %x %f %d %d %d %d %d %d %d %d",
		&(e.IfName),
		&(e.Status),
		&(e.Qual),
		&(e.Level),
		&(e.Noise),
		&(e.Discarded.Nwid),
		&(e.Discarded.Crypt),
		&(e.Discarded.Frag),
		&(e.Discarded.Retry),
		&(e.Discarded.Misc),
		&(e.Missed))
	e.IfName = strings.TrimRight(e.IfName, ":")
	return e, err

}

func readEntry(r io.Reader, name string) (Entry, error) {
	var e Entry

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := strings.TrimLeft(scanner.Text(), " ")
		if s, found := strings.CutPrefix(line, name); found {
			return scanEntry(name + s)
		}
	}
	return e, scanner.Err()
}

func GetStats(name string) (Entry, error) {
	f, err := os.Open(procFile)
	if err != nil {
		return Entry{}, err
	}
	defer f.Close()
	return readEntry(f, name)
}
