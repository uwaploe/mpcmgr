// Package calculates the filtered foward difference of input data values
package fwddiff

import (
	"time"
)

type Filter interface {
	Update(float64) float64
}

type Diff struct {
	t      time.Time
	x      float64
	filter Filter
}

func New(f Filter) *Diff {
	return &Diff{filter: f}
}

// Next returns the updated forward difference estimate delta_x/delta_t
func (l *Diff) Next(x float64) float64 {
	if l.t.IsZero() {
		l.t = time.Now()
		l.x = x
		return l.filter.Update(0)
	}
	t := time.Now()
	dt := t.Sub(l.t)
	y := l.filter.Update((x - l.x) / dt.Seconds())
	l.t, l.x = t, x
	return y
}
